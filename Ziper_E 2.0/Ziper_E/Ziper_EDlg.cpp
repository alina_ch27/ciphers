
// Ziper_EDlg.cpp : ���� ����������
//
#include "stdafx.h"
#include "Ziper_E.h"
#include "Ziper_EDlg.h"
#include "afxdialogex.h"

#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <string>




using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//���� 1
const int ip[64] = { 58, 50, 42, 34, 26, 18, 10, 2,
60, 52, 44, 36, 28, 20, 12, 4,
62, 54, 46, 38, 30, 22, 14, 6,
64, 56, 48, 40, 32, 24, 16, 8,
57, 49, 41, 33, 25, 17, 9, 1,
59, 51, 43, 35, 27, 19, 11, 3,
61, 53, 45, 37, 29, 21, 13, 5,
63, 55, 47, 39, 31, 23, 15, 7 };
//���� 2
const int expansion[48] = { 32, 1, 2, 3, 4, 5, 4, 5,
6, 7, 8, 9, 8, 9, 10, 11,
12, 13, 12, 13, 14, 15, 16, 17,
16, 17, 18, 19, 20, 21, 20, 21,
22, 23, 24, 25, 24, 25, 26, 27,
28, 29, 28, 29, 30, 31, 32, 1 };
//������� 3 ������������
const int Sj[8][64] = {
	{ 14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
	0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
	4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
	15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13 },

	{ 15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
	3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
	0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
	13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9 },

	{ 10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
	13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
	13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
	1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12 },

	{ 7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
	13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
	10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
	3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14 },

	{ 2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
	14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
	4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
	11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3 },

	{ 12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
	10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
	9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 1, 6,
	4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 1 },

	{ 4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
	13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
	1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
	6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12 },

	{ 13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
	1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
	7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
	2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11 }
};
//���� 4
const int P[32] =
{
	16, 7, 20, 21, 29, 12, 28, 17,
	1, 15, 23, 26, 5, 18, 31, 10,
	2, 8, 24, 14, 32, 27, 3, 9,
	19, 13, 30, 6, 22, 11, 4, 25
};


//���� 5

const int IP_for_key[56] = { 57, 49, 41, 33, 25, 17, 9, 1,
58, 50, 42, 34, 26, 18, 10, 2,
59, 51, 43, 35, 27, 19, 11, 3,
60, 52, 44, 36, 63, 55, 47, 39,
31, 23, 15, 7, 62, 54, 46, 38,
30, 22, 14, 6, 61, 53, 45, 37,
29, 21, 13, 5, 28, 20, 12, 4 };
//���� 6
const int roker_key[48] = { 14, 17, 11, 24, 1, 5,
3, 28, 15, 6, 21, 10,
23, 19, 12, 4, 26, 8,
16, 7, 27, 20, 13, 2,
41, 52, 31, 37, 47, 55,
30, 40, 51, 45, 33, 48,
44, 49, 39, 56, 34, 53,
46, 42, 50, 36, 29, 32 };
//���� 7

const int IP_1[64] = { 40, 8, 48, 16, 56, 24, 64, 32,
39, 7, 47, 15, 55, 23, 63, 31,
38, 6, 46, 14, 54, 22, 62, 30,
37, 5, 45, 13, 53, 21, 61, 29,
36, 4, 44, 12, 52, 20, 60, 28,
35, 3, 43, 11, 51, 19, 59, 27,
34, 2, 42, 10, 50, 18, 58, 26,
33, 1, 41, 9, 49, 17, 57, 25 };


CZiper_EDlg::CZiper_EDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_ZIPER_E_DIALOG, pParent)
	, Key(_T(""))
	, Des(FALSE)
	, Rsa(FALSE)
	, Key_DES(_T(""))
	, nr(0)
	, er(0)
	, dr(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CZiper_EDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_NOTSHR, OpenText);
	DDX_Text(pDX, IDC_SHR, CipherText);
	DDX_Text(pDX, IDC_GEN_KEYR, Key);
	DDX_Check(pDX, IDC_DESR, Des);
	DDX_Text(pDX, IDC_DESHIFR, DeCipherText);
	DDX_Check(pDX, IDC_RSAR, Rsa);
	DDX_Text(pDX, IDC_KEY_DES, Key_DES);
	DDX_Text(pDX, IDC_NR, nr);
	DDX_Text(pDX, IDC_ER, er);
	DDX_Text(pDX, IDC_DR, dr);
}

BEGIN_MESSAGE_MAP(CZiper_EDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_GEN_KEY, &CZiper_EDlg::OnBnClickedGenKey)
	ON_BN_CLICKED(IDC_OPEN_NOTSH, &CZiper_EDlg::OnBnClickedOpenNotsh)
	ON_BN_CLICKED(IDC_OPEN_SH, &CZiper_EDlg::OnBnClickedOpenSh)
	ON_BN_CLICKED(IDC_SAVE_SH, &CZiper_EDlg::OnBnClickedSaveSh)
	ON_BN_CLICKED(IDC_SAVE_NOTSH, &CZiper_EDlg::OnBnClickedSaveNotsh)
	ON_BN_CLICKED(IDC_SHIFRR, &CZiper_EDlg::OnBnClickedShifrr)
	ON_BN_CLICKED(IDC_DESHIFRR, &CZiper_EDlg::OnBnClickedDeshifrr)
	
END_MESSAGE_MAP()


// ����������� ��������� CZiper_EDlg

BOOL CZiper_EDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CZiper_EDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CZiper_EDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


vector <unsigned char> Isk_or(CString data, unsigned char *key)//����������� ���
{

	vector <unsigned char> res;
	vector <unsigned char> text;
	int razmer1 = data.GetLength();
	if (razmer1 % 8 != 0)
	{
		for (int i = 0; i < (8 - razmer1 % 8); i++)
			data += '\0'; 	
	}
	
	int razmer = data.GetLength();	
	res.resize(razmer); 	
	int f = 0;
	for (int i = 0; i < data.GetLength(); i++)
	{
		if (f < 8)
		{
			res[i] = data[i] ^ key[f];
			f++;
		}
		else res[i] = data[i] ^ key[i % 8];		     
	}
	return res;
}

void bool_to_ten(std::vector <bool> &result, int *cipher)//�� ���� � ���������� 
{
	for (int k = 0; k < result.size() / 8; k++)
	{
		cipher[k] = (int)(result[7 + 8 * k] * 1 + result[6 + 8 * k] * 2 + result[5 + 8 * k] * 2 * 2 + result[4 + 8 * k] * 2 * 2 * 2 + result[3 + 8 * k] * 2 * 2 * 2 * 2 + result[2 + 8 * k] * 2 * 2 * 2 * 2 * 2 + result[1 + 8 * k] * 2 * 2 * 2 * 2 * 2 * 2 + result[0 + 8 * k] * 2 * 2 * 2 * 2 * 2 * 2 * 2);
	}
}

void CZiper_EDlg::String_to_BOOL()//��������� ������ � ���
{
	int length = OpenText.GetLength();

	if (length % 8 != 0)
	{
		for (int i = 0; i < (8 - length % 8); i++)
			OpenText += '\0';
	}

	int *buffer = new int[OpenText.GetLength()];
	for (int i = 0; i < OpenText.GetLength(); i++)
	{
		buffer[i] = OpenText.GetAt(i);
	}

	bool *buffer_bool = new bool[OpenText.GetLength() * 8];
	text.clear();
	text.resize(OpenText.GetLength() * 8);

	int delimoe;
	for (int i = 0; i < OpenText.GetLength(); i++)
	{
		delimoe = buffer[i];
		for (int j = 0; j < 8; j++)
		{
			buffer_bool[i * 8 + j] = delimoe % 2;
			delimoe = delimoe / 2;
		}

		for (int j = 0; j < 8; j++)
		{
			text[i * 8 + j] = buffer_bool[i * 8 + 7 - j];
		}
	}
	delete[] buffer;
	delete[] buffer_bool;


}

void CZiper_EDlg::key_to_BOOL()//��������� ����  � ���
{
	int *buffer = new int[8];
	for (int i = 0; i < 8; i++)
	{
		buffer[i] = Key.GetAt(i);
	}

	bool *buffer_bool = new bool[64];
	key.clear();
	key.resize(64);

	int delimoe;
	for (int i = 0; i < 8; i++)
	{
		delimoe = buffer[i];
		for (int j = 0; j < 8; j++)
		{
			buffer_bool[i * 8 + j] = delimoe % 2;
			delimoe = delimoe / 2;
		}

		for (int j = 0; j < 8; j++)
		{
			key[i * 8 + j] = buffer_bool[i * 8 + 7 - j];
		}
	}

	delete[] buffer;
	delete[] buffer_bool;
}

void CZiper_EDlg::String_cipher_to_BOOL()//��������� ����������� ����� � ���
{

	int razmer = CipherText.GetLength();

	if (razmer % 8 != 0)
	{
		for (int i = 0; i < (8 - razmer % 8); i++)
			CipherText += '\0';
	}

	int *buffer = new int[CipherText.GetLength()];
	for (int i = 0; i < CipherText.GetLength(); i++)
	{
		buffer[i] = CipherText.GetAt(i);
	}

	bool *buffer_bool = new bool[CipherText.GetLength() * 8];
	cipher_text.clear();
	cipher_text.resize(CipherText.GetLength() * 8);


	int delimoe;
	for (int i = 0; i < CipherText.GetLength(); i++)
	{
		delimoe = buffer[i];
		for (int j = 0; j < 8; j++)
		{
			buffer_bool[i * 8 + j] = delimoe % 2;
			delimoe = delimoe / 2;
		}

		for (int j = 0; j < 8; j++)
		{
			cipher_text[i * 8 + j] = buffer_bool[i * 8 + 7 - j];
		}
	}
	delete[] buffer;
	delete[] buffer_bool;


}
/////////////RSA
int CZiper_EDlg::gcd(int a, int b)
{
	if (b == 0) return a;
	else return gcd(b, a%b);
}    

bool CZiper_EDlg::test_ferma(long long int x)
{

	for (int i = 0; i < 100; i++)
	{
		int a = rand();
		if (gcd(a, x) != 1)
			return false;
		if (step_mod(a, x - 1, x) != 1)
			return false;
	}
	return true;
}

long long int CZiper_EDlg::evclid(long long int a, long long int b, long long int *xx, long long int *yy) // ����������� �������� �������
{

	long long x_last1 = 1, x_0 = 0, y_last1 = 0, y_0 = 1, x, y;
	while (b > 0)
	{
		long long int q = a / b;
		long long int r = a%b;
		a = b;
		b = r;
		x = x_last1 - q*x_0;
		y = y_last1 - q*y_0;

		x_last1 = x_0;
		y_last1 = y_0;
		x_0 = x;
		y_0 = y;
	}

	*xx = x_last1;
	*yy = y_last1;
	return a;
}

unsigned __int64 CZiper_EDlg::step_mod(long long int base, long long int exp, long long int module) // ���������� � ������� �� ������
{
	unsigned __int64 result = 1;
	if (exp == 0) return result;
	while (exp >= 1)
	{
		if ((exp % 2) == 1)
		{
			result = result%module;
			result *= base%module;
			exp = exp / 2;
			base = ((base % module)*(base%module)) % module;
		}
		else
		{
			exp = exp / 2;
			base = ((base % module)*(base%module)) % module;
		}
	}

	return result% module;
}
////////////////////////////////////
unsigned char Key_g[8];

void CZiper_EDlg::OnBnClickedGenKey()
{
	UpdateData(TRUE);
	int ed = 0;
	Key = " "; CString Text_B;
	for (int i = 0; i < 8; i++)
	{
		Key_g[i] = rand() % 256;
		Key += Key_g[i];
	}
	if (Des)
	{
		key_to_BOOL();
		int n;
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 7; j++)
			{
				if (key[i * 8 + j] == true)
					ed++;
			}
			if (ed % 2 == 0) key[i * 8 + 7] = true;
			else key[i * 8 + 7] = false;
			ed = 0;
		}
		int *cipher = new int[8];
		bool_to_ten(key, cipher);
		Key_DES = " ";
		for (int k = 0; k < 8; k++)
		{
			Key_DES.AppendChar(cipher[k]);
		}
	}
	if (Rsa)
	{
		int n;
		// ��������� ���� ����� � ����� ���� ������� �����.
		long long int p, q, e, x = 0, y = 0, d;;
		do{ p = (rand() + rand() + rand()) % 35317; } while (!test_ferma(p));
		do{ q = (rand() + rand() + rand()) % 35317; } while (!test_ferma(q));


		n = p*q;
		nr = n;
		long long int fi = (p - 1)*(q - 1);

		do{ e = (rand() + rand()) % fi; } while (gcd(e, fi) != 1);

		d = evclid(e, fi, &x, &y);

		er = e;
		if (x<0) dr = x%fi + fi;
		else
			dr = x%fi;
	}
	
	UpdateData(FALSE);
}



vector <unsigned char> result;
string buf_s;
string buf_ds;
string buf_ns;

void CZiper_EDlg::OnBnClickedShifrr()
{
	UpdateData(TRUE);

	buf_s = "";
	if ((Des == FALSE) & (Rsa==FALSE))
	{
		result = Isk_or(OpenText, Key_g);

		for (int k = 0; k < result.size(); k++)
		{
			CipherText += result[k];
		}
	}
	if (Des)
	{
		CipherText.Empty();
		String_to_BOOL();

		int len_ns = OpenText.GetLength();
		bool *buffer = new bool[len_ns * 8];//����� ��� ����� �������� �������������� �������
	

		for (int i = 0; i < len_ns / 8; i++)
		{
			for (int j = 0; j < 64; j++)
			{
				buffer[i * 64 + j] = text[i * 64 + ip[j] - 1];
			}
		}

		for (int i = 0; i < len_ns * 8; i++)//�������� ������� ����� ������ ������������
		{
			text[i] = buffer[i];
		}

		delete[] buffer;


		key_to_BOOL();


		bool *buffer_key = new bool[56];

		for (int j = 0; j < 56; j++)
		{
			buffer_key[j] = key[IP_for_key[j] - 1];
		}


		for (int i = 0; i < 56; i++)
		{
			key[i] = buffer_key[i];

		}
		delete[] buffer_key;


		for (int i = 0; i < 28; i++)//����� ���� �� ��� �����
		{
			C[0][i] = key[i];
			D[0][i] = key[i + 28];
		}
		//����� ������ ��������� ������ 

		for (int i = 1; i < 3; i++)
		{

			for (int j = 0; j < 28; j++)
			{
				if ((j + 1 > 27)) C[i][j] = C[i - 1][0];
				C[i][j] = C[i - 1][j + 1];
			}

			for (int j = 0; j < 28; j++)
			{
				if ((j + 1 > 27)) D[i][j] = D[i - 1][0];
				D[i][j] = D[i - 1][j + 1];
			}
		}


		for (int i = 3; i < 9; i++)
		{
			for (int j = 0; j < 28; j++)
			{
				if ((j + 2 > 27)) C[i][j] = C[i - 1][27 - j - 2];
				C[i][j] = C[i - 1][j + 2];
			}

			for (int j = 0; j < 28; j++)
			{
				if ((j + 2 > 27)) D[i][j] = D[i - 1][27 - j - 2];
				D[i][j] = D[i - 1][j + 2];
			}
		}


		for (int j = 0; j < 28; j++)
		{
			if ((j + 1 > 27)) C[9][j] = C[8][0];
			C[9][j] = C[8][j + 1];
		}

		for (int j = 0; j < 28; j++)
		{
			if ((j + 1 > 27)) D[9][j] = D[8][0];
			D[9][j] = D[8][j + 1];
		}


		for (int i = 10; i < 16; i++)
		{
			for (int j = 0; j < 28; j++)
			{
				if ((j + 2 > 27)) C[i][j] = C[i - 1][27 - j - 2];
				C[i][j] = C[i - 1][j + 2];
			}

			for (int j = 0; j < 28; j++)
			{
				if ((j + 2 > 27)) D[i][j] = D[i - 1][27 - j - 2];
				D[i][j] = D[i - 1][j + 2];
			}
		}


		for (int j = 0; j < 28; j++)
		{
			if ((j + 1 > 27)) C[16][j] = C[15][0];
			C[16][j] = C[15][j + 1];
		}

		for (int j = 0; j < 28; j++)
		{
			if ((j + 1 > 27)) D[16][j] = D[15][0];
			D[16][j] = D[15][j + 1];
		}

		//���������� ����� �� ����-�� �i � Di
		for (int i = 0; i < 16; i++)
		{
			bool *buf = new bool[56];
			for (int k = 0; k < 56; k++)
			{
				if (k < 28) buf[k] = C[i + 1][k];
				else buf[k] = D[i + 1][k - 28];
			}

			for (int k = 0; k < 48; k++)
			{
				keys_48bit[i][k] = buf[roker_key[k] - 1];
			}
			delete[]buf;
		}




		T.clear();
		T.resize(len_ns * 8);

		for (int i = 0; i < text.size() / 64; i++)
		{
			bool *L = new bool[32];
			bool *R = new bool[32];


			for (int k = 0; k < 32; k++)
			{
				L[k] = text[i * 64 + k];
				R[k] = text[i * 64 + 32 + k];
			}

			//��������� R � 32 �� 48
			for (int ii = 1; ii < 17; ii++)
			{
				bool *E = new bool[48];
				for (int k = 0; k < 48; k++)
				{
					E[k] = R[expansion[k] - 1];
				}
				//�������� ����������� � � ������
				for (int k = 0; k < 48; k++)
				{
					E[k] = E[k] ^ keys_48bit[ii - 1][k];
				}
				//������������ � ���� 6-������ ������
				bool **B = new bool *[8];
				for (int k = 0; k < 8; k++)
					B[k] = new bool[6];

				for (int k = 0; k < 8; k++)
				{
					for (int j = 0; j < 6; j++)
					{
						B[k][j] = E[k * 6 + j];
					}
				}

				delete[]E;
				//l���� ������ �� ������ Bj ���������������� � 4-������� ���� Bj � ������� ���������� �������  
				bool **BB = new bool*[8];
				for (int k = 0; k < 8; k++)
					BB[k] = new bool[4];


				for (int k = 0; k < 8; k++)
				{
					int a, b, c;

					a = B[k][5] * 1 + B[k][0] * 2;
					b = B[k][1] * 2 * 2 * 2 + B[k][2] * 2 * 2 + B[k][3] * 2 + B[k][4] * 1;
					c = Sj[k][a * 16 + b];

					for (int j = 0; j < 4; j++)
					{
						BB[k][3 - j] = c % 2;
						c = c / 2;
					}
				}

				for (int k = 0; k < 8; k++) delete[] B[k];
				delete[] B;

				bool *f = new bool[32];
				bool *buf = new bool[32];
				for (int k = 0; k < 8; k++)
				{
					for (int j = 0; j < 4; j++)
					{
						buf[k * 4 + j] = BB[k][j];
					}
				}

				for (int k = 0; k < 8; k++)
					delete[] BB[k];
				delete[] BB;
				//�������� ������� ����������
				for (int k = 0; k < 32; k++)
				{
						f[k] = buf[P[k] - 1];
				}
				delete[] buf;
				//��������� ���� ����������

				bool *L_past = new bool[32];
				for (int k = 0; k < 32; k++)
				{
					L_past[k] = L[k];
					L[k] = R[k];
				}

				for (int k = 0; k < 32; k++)
				{
					R[k] = L_past[k] ^ f[k];
				}
				delete[] f;
				delete[]L_past;
			}

			//���������� ���������� ��������������
			bool *T_buf = new bool[64];
			for (int k = 0; k < 64; k++)
			{
				if (k < 32) T_buf[k] = L[k];
				else T_buf[k] = R[k - 32];
			}

			for (int k = 0; k < 64; k++)
			{
				T[i * 64 + k] = T_buf[IP_1[k] - 1];
			}

			delete[] T_buf;
			delete[]L;
			delete[] R;


		}

		int *cipher = new int[T.size() / 8];

		bool_to_ten(T, cipher);
	
		for (int k = 0; k < T.size() / 8; k++)
		{

			CipherText.AppendChar(cipher[k]);
		}
		delete[] cipher;
		

	}
	if (Rsa)
	{
		CipherText.Empty();
		int len_ns = OpenText.GetLength();	
		public_text.clear();
		cipher_text1.clear();
		public_text.resize(len_ns);
		cipher_text1.resize(len_ns);
		for (int k = 0; k < len_ns; k++)
		{
			CString str;
			public_text[k] = OpenText.GetAt(k);

			cipher_text1[k] = step_mod(public_text[k], er, nr);

			str.Format(_T("%d"), cipher_text1[k]);


			CipherText.AppendChar(cipher_text1[k]);
		}
	}
	UpdateData(FALSE);
}


void CZiper_EDlg::OnBnClickedDeshifrr()
{
	UpdateData(TRUE);
	buf_ds = "";
	if ((Des == FALSE) & (Rsa == FALSE))
	{
		result = Isk_or(CipherText, Key_g);

		for (int k = 0; k < result.size(); k++)
		{
			DeCipherText += result[k];
		}
	}
	if (Des)
	{
		int len_ci = CipherText.GetLength();
		String_cipher_to_BOOL();
		decipher_text.clear();
		decipher_text.resize(len_ci * 8);




		for (int i = 0; i < cipher_text.size() / 64; i++)
		{
			bool *L = new bool[32];
			bool *R = new bool[32];
			bool *T_buf = new bool[64];

			for (int k = 0; k < 64; k++)
			{
					T_buf[IP_1[k] - 1] = cipher_text[k + i * 64];
			}

			for (int k = 0; k < 64; k++)
			{
				if (k < 32) L[k] = T_buf[k];
				else R[k - 32] = T_buf[k];
			}
			delete[] T_buf;


			for (int ii = 16; ii>0; ii--)
			{
				bool *E = new bool[48];
				for (int k = 0; k < 48; k++)
				{
			      	E[k] = L[expansion[k] - 1];
				}

				for (int k = 0; k < 48; k++)
				{
					E[k] = E[k] ^ keys_48bit[ii - 1][k];
				}

				bool **B = new bool *[8];
				for (int k = 0; k < 8; k++) B[k] = new bool[6];

				for (int k = 0; k < 8; k++)
				{
					for (int j = 0; j < 6; j++)
					{
						B[k][j] = E[k * 6 + j];
					}
				}

				delete[]E;

				bool **BB = new bool*[8];
				for (int k = 0; k < 8; k++) BB[k] = new bool[4];


				for (int k = 0; k < 8; k++)
				{
					int a, b, c;
					a = B[k][5] * 1 + B[k][0] * 2;
					b = B[k][1] * 2 * 2 * 2 + B[k][2] * 2 * 2 + B[k][3] * 2 + B[k][4] * 1;
					c = Sj[k][a * 16 + b];

					for (int j = 0; j < 4; j++)
					{
						BB[k][3 - j] = c % 2;
						c = c / 2;
					}
				}

				for (int k = 0; k < 8; k++) delete[] B[k];
				delete[] B;

				bool *f = new bool[32];
				bool *buf = new bool[32];
				for (int k = 0; k < 8; k++)
				{
					for (int j = 0; j < 4; j++)
					{
						buf[k * 4 + j] = BB[k][j];
					}
				}

				for (int k = 0; k < 8; k++) delete[] BB[k];
				delete[] BB;

				for (int k = 0; k < 32; k++)
				{
						f[k] = buf[P[k] - 1];
				}
				delete[] buf;


				bool *R_past = new bool[32];
				for (int k = 0; k < 32; k++)
				{
					R_past[k] = R[k];
					R[k] = L[k];
				}

				for (int k = 0; k < 32; k++)
				{
					L[k] = R_past[k] ^ f[k];
				}
				delete[] f;
				delete[]R_past;
			}

			for (int k = 0; k < 64; k++)
			{
				if (k < 32) decipher_text[i * 64 + k] = L[k];
				else decipher_text[i * 64 + k] = R[k - 32];
			}

			delete[] R;
			delete[]L;

		}



		bool *buffer = new bool[len_ci * 8];
		for (int i = 0; i < len_ci / 8; i++)
		{
			for (int j = 0; j < 64; j++)
			{
				buffer[i * 64 + ip[j] - 1] = decipher_text[i * 64 + j];
			}
		}

		for (int i = 0; i < OpenText.GetLength() * 8; i++)
		{
			decipher_text[i] = buffer[i];
		}
		delete[]buffer;

		int *decipher = new int[decipher_text.size() / 8];
		bool_to_ten(decipher_text, decipher);
		
		for (int k = 0; k < decipher_text.size() / 8; k++)
		{
			DeCipherText.AppendChar(decipher[k]);
		}
		delete[] decipher;
	} 
	if (Rsa)
	{
		int len_s = CipherText.GetLength();
         DeCipherText.Empty();
		 decipher_text1.clear();
		 decipher_text1.resize(CipherText.GetLength());
		 cipher_text1.resize(CipherText.GetLength());
		 for (int k = 0; k < CipherText.GetLength(); k++)
		 {

			 decipher_text1[k] = step_mod(cipher_text1[k], dr, nr);

			 CString str;
			 str.Format(_T("%d"), decipher_text1[k]);

			 DeCipherText.AppendChar(decipher_text1[k]);
		 }
		
	}
		UpdateData(FALSE);
	
}

/// open and save
void CZiper_EDlg::OnBnClickedOpenNotsh()
{
	// TODO: �������� ���� ��� ����������� �����������
	text_nosh = "";
	text_sh = "";

	TCHAR szFilters[] = _T("��������� ��������� (*.txt)|*.txt|��� ����� (*.*)|*.*||");
	CFileDialog fLoadCodeDlg(TRUE, _T("txt"), _T("*.txt"), OFN_FILEMUSTEXIST || OFN_HIDEREADONLY, szFilters);
	string  text;//coded=text_nosh
	CString strLine;
	if (fLoadCodeDlg.DoModal() == IDOK)
	{//����� ���� ������
		Name_nosh = fLoadCodeDlg.GetPathName();

		CStdioFile fLoadText;
		if (fLoadText.Open((LPCTSTR)Name_nosh, CFile::modeRead | CFile::typeText)) {
			while (true) {
				if (!fLoadText.ReadString(strLine) && strLine.IsEmpty()) break;
				else text_nosh += strLine + _T("\r\n");
			}
			fLoadText.Close();
		}
		else
		{
			MessageBox((LPCTSTR)_T("������!!!\r\n�������, �������� ���� �� ��������."), (LPCTSTR)_T("������"));
			return;
			fLoadText.Close();
		}

	}
	else MessageBox((LPCTSTR)_T("������!!!\r\n�������, �������� ���� �� ��������."), (LPCTSTR)_T("������"));
	//double R = Decoder(Coded, text);
	CString Text;
	//Text = text.c_str();
	OpenText = text_nosh;

	UpdateData(FALSE);
}




void CZiper_EDlg::OnBnClickedOpenSh()
{
	// TODO: �������� ���� ��� ����������� �����������
	text_nosh = "";
	text_sh = "";

	TCHAR szFilters[] = _T("��������� ��������� (*.txt)|*.txt|��� ����� (*.*)|*.*||");
	CFileDialog fLoadCodeDlg(TRUE, _T("txt"), _T("*.txt"), OFN_FILEMUSTEXIST || OFN_HIDEREADONLY, szFilters);
	CString strLine;
	if (fLoadCodeDlg.DoModal() == IDOK)
	{//����� ���� ������
		Name_sh = fLoadCodeDlg.GetPathName();

		CStdioFile fLoadText;
		if (fLoadText.Open((LPCTSTR)Name_sh, CFile::modeRead | CFile::typeText)) {
			while (true) {
				if (!fLoadText.ReadString(strLine) && strLine.IsEmpty()) break;
				else text_sh += strLine + _T("\r\n");
			}
			fLoadText.Close();
		}
		else
		{
			MessageBox((LPCTSTR)_T("������!!!\r\n�������, �������� ���� �� ��������."), (LPCTSTR)_T("������"));
			return;
			fLoadText.Close();
		}

	}
	else MessageBox((LPCTSTR)_T("������!!!\r\n�������, �������� ���� �� ��������."), (LPCTSTR)_T("������"));
	//double R = Decoder(Coded, text);
	CString Text;
	//Text = text.c_str();
	CipherText = text_sh;

	UpdateData(FALSE);
}
void CZiper_EDlg::OnBnClickedSaveSh()
{
	buf_s = "";
	TCHAR szFilters[] = _T("��������� ��������� (*.txt)|*.txt|��� ����� (*.*)|*.*||");
	CFileDialog fLoadCodeDlg(FALSE, _T("txt"), _T("*.txt"), OFN_FILEMUSTEXIST || OFN_HIDEREADONLY, szFilters);
	CString strLine;
	for (int i = 0; i < CipherText.GetLength(); i++)
	{
		buf_s+= CipherText[i];
	}
	if (fLoadCodeDlg.DoModal() == IDOK)
	{//����� ���� soxpaneniya
		NameS_sh = fLoadCodeDlg.GetPathName();
		CStdioFile fSaveCode;
		if (fSaveCode.Open((LPCTSTR)(NameS_sh), CFile::modeCreate | CFile::modeWrite | CFile::typeText)) {
			for (int i = 0; i <buf_s.size(); i++)
				fSaveCode.Write(&(buf_s[i]), 1);
		}
		fSaveCode.Close();
	}
	else MessageBox((LPCTSTR)_T("������!!!\r\n�������, �������� ���� �� ��������."), (LPCTSTR)_T("������"));


}


void CZiper_EDlg::OnBnClickedSaveNotsh()
{
	buf_ds = "";
	TCHAR szFilters[] = _T("��������� ��������� (*.txt)|*.txt|��� ����� (*.*)|*.*||");
	CFileDialog fLoadCodeDlg(FALSE, _T("txt"), _T("*.txt"), OFN_FILEMUSTEXIST || OFN_HIDEREADONLY, szFilters);
	string  text;//coded=text_nosh
	CString strLine;
	for (int i = 0; i < DeCipherText.GetLength(); i++)
	{
		buf_ds += DeCipherText[i];
	}
	if (fLoadCodeDlg.DoModal() == IDOK)
	{//����� ���� soxpaneniya
		NameS_nosh = fLoadCodeDlg.GetPathName();
		CStdioFile fSaveCode;
		if (fSaveCode.Open((LPCTSTR)(NameS_nosh), CFile::modeCreate | CFile::modeWrite | CFile::typeText)) {
			for (int i = 0; i < buf_ds.size(); i++)
				fSaveCode.Write(&(buf_ds[i]), 1);
		}
		fSaveCode.Close();
	}
	else MessageBox((LPCTSTR)_T("������!!!\r\n�������, �������� ���� �� ��������."), (LPCTSTR)_T("������"));
}



	
	


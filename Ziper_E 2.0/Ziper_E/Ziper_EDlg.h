
// Ziper_EDlg.h : ���� ���������
//

#pragma once
#include "afxwin.h"
#include <vector>

using namespace std;
// ���������� ���� CZiper_EDlg
class CZiper_EDlg : public CDialogEx
{
// ��������
public:
	CZiper_EDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ZIPER_E_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	
	afx_msg void OnBnClickedGenKey();
	CString OpenText;
	CString CipherText;
	CString DeCipherText;
	CString Key;

	CString text_nosh;
	CString text_sh;
	CString Name_sh;
	CString Name_nosh;
	CString NameS_sh;
	CString NameS_nosh;

	std::vector <bool> text;
	std::vector <bool> key;
	std::vector <bool> cipher_text;
	std::vector <bool> decipher_text;

	bool keys_48bit[16][48];
	bool C[17][28];
	bool D[17][28];

	std::vector <bool> T;
	std::vector <bool>Text1;
	std::vector <bool>Textch;
	std::vector <bool>Textdech;

	std::vector <int> public_text;
	std::vector <int> cipher_text1;
	std::vector <int> decipher_text1;
	int gcd(int a, int b);
	bool test_ferma(long long int x);
	unsigned __int64 step_mod(long long int base, long long int exp, long long int module);
	long long int evclid(long long int a, long long int b, long long int *x, long long int *y);

	void String_to_BOOL();
	void String_cipher_to_BOOL();
	void key_to_BOOL();


	afx_msg void OnBnClickedOpenNotsh();
	afx_msg void OnBnClickedOpenSh();
	afx_msg void OnBnClickedSaveSh();
	afx_msg void OnBnClickedSaveNotsh();
	BOOL Des;
	afx_msg void OnBnClickedShifrr();
	afx_msg void OnBnClickedDeshifrr();
	
	BOOL Rsa;
	CString Key_DES;
	afx_msg void OnBnClickedGenrsa();
	int nr;
	int er;
	int dr;
};
